import { EntityRepository, Repository } from "typeorm";
import { Offers } from "./offers.entity";

/**
 * @author Rashmi Gupta
 * Offers Repository
 */
@EntityRepository(Offers)
export class OffersRepository extends Repository<Offers>{ }