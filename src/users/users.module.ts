import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./users.entity";
import { UserRepository } from "./users.repository";
import { UserService } from "./users.service";
import { UserController } from "./userscontroller";

/**
 * @author Rashmi Gupta
 * @Module User Module
 *imported Modules -PassportModule,JwtModule,TypeOrmModule
 */
@Module({
    imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.registerAsync({
            useFactory: () => ({
                secret: process.env.JWT_SECRET,
                signOptions: {
                    expiresIn: '1d'
                }
            })
        }),
        TypeOrmModule.forFeature([User, UserRepository])
    ],
    controllers: [UserController],
    providers: [UserService]
})
export class UserModule { } 