import { Injectable, InternalServerErrorException, Logger, NotFoundException } from "@nestjs/common";
import { FETCHED_SUCCESSFULLY } from "../constant";
import { TransactionsRepository } from "./transactions.repository";
import { TransactionsDTO } from "./transactions.dto";

/**
 * @author Rashmi Gupta
 * Transactions Service
 */
@Injectable()
export class TransactionsService {

    /**
     * Logger
     */
    logger = new Logger(TransactionsService.name);

    /**
     * Dependency
     * @param transactionsService TransactionsRepository
     */
    constructor(private readonly transactionsRepo: TransactionsRepository) { }

    /**
     * Transcation By Id
     * @param id number
     * @returns Success or failure
     */
    async transcationById(id: number): Promise<TransactionsDTO> {
        try {
            let response = await this.transactionsRepo.findOne(id);
            if (response) {
                const msg: string = ` Transaction with ${id} ${FETCHED_SUCCESSFULLY} `;
                return response;
            } else {
                const msg = `Benificiary Not Found`;
                throw new NotFoundException(msg);
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message)
        }
    }
    
} 