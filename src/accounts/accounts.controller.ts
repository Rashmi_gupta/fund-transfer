import { Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Post } from "@nestjs/common";
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse } from "@nestjs/swagger";
import { ACCOUNT_CREATED_SUCCESS, ACCOUNT_CREATION_FAILED, ACCOUNT_FETCHED_BY_ID, ACCOUNT_FETCHED_BY_ID_FAILID } from "../constant";
import { AccountsDTO } from "./accounts.dto";
import { AccountsService } from "./accounts.service";

/**
 * @author Rashmi Gupta
 * Accounts Controller
 */
@Controller('user')
export class AccountsController {

  /**
   * Dependency 
   * @param accountsService AccountsService
   */
  constructor(private readonly accountsService: AccountsService) { }

  /**
   * Create Account
   * @param accounts AccountsDTO
   * @returns Success or Failure
   */
  @ApiCreatedResponse({ description: ACCOUNT_CREATED_SUCCESS, status: HttpStatus.CREATED })
  @ApiInternalServerErrorResponse({ description: ACCOUNT_CREATION_FAILED, status: HttpStatus.INTERNAL_SERVER_ERROR })
  @Post("createAccount")
  createAccount(@Body() accounts: AccountsDTO) {
    return this.accountsService.createAccount(accounts);
  }

  /**
   * Get Account By Id
   * @param id number
   * @returns Success or Failure
   */
  @ApiOkResponse({ description: ACCOUNT_FETCHED_BY_ID, status: HttpStatus.OK })
  @ApiInternalServerErrorResponse({ description: ACCOUNT_FETCHED_BY_ID_FAILID, status: HttpStatus.INTERNAL_SERVER_ERROR })
  @Get(':id/account')
  getAccountById(@Param('id', ParseIntPipe) id: number): Promise<any> {
    return this.accountsService.getAccountById(id);
  }

/**
 * Delete Account By Id
 * @param id number
 * @returns Success or Failure
 */
  @Delete(':id/deleteAccount')
  deleteAccountById(@Param('id',ParseIntPipe)id:number){
    return this.accountsService.deleteAccountById(id);
  }

}