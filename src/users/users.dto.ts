import { IsEmail, IsNotEmpty } from "class-validator";

/**
 * @author Rashmi Gupta
 * User DTO
 */
export class UserDTO {

    /**
     * @type number
     * UserId(primary genreted)
     */
    id: number;

    /**
     * @type string
     * userName should not be empty
     */
    @IsNotEmpty({ message: 'User Name can not be empty' })
    userName: string;

    /**
     *  @type string
     * email Id should not be empty
     * email Id should be Unique
     */
    @IsNotEmpty({ message: 'emailId  can not be empty' })
    @IsEmail({ messge: 'emailId should be a email' })
    emailId: string;

    /**
     * @type string
     * password should not be empty
     */
    @IsNotEmpty({ message: 'Password  can not be empty' })
    password: string;

    /**
     * @type number
     *phoneNumber should not be empty
     */
    @IsNotEmpty({ message: 'phoneNumber can not be empty' })
    phoneNumber: number;

    /**
     *  @type string
     * address should not be empty
     */
    @IsNotEmpty({ message: 'address can not be empty' })
    address: string;

}  