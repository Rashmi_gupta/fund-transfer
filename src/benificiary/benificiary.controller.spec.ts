import { Test } from "@nestjs/testing";
import { BenificiaryController } from "./benificiary.controller";
import { BenificiaryService } from "./benificiary.service";

const addBenficiary = "BENIFICIARY ADDED SUCCESSFULLY WITH iD : 6";

const benficiary =
{
    id: 1,
    user:null,
    benificiaryIFSC: "1234IFSC",
    status: "active",
    benificiaryAccountNumber: 123456
  };

 const deleteBenificiary =  "BENIFICIARY DELETED SUCCESSFULLY WITH ID :3";


describe('Given BenificiaryController', () =>{
let benificiaryController: BenificiaryController;
let benificiaryService:BenificiaryService;

beforeEach(async ()=>{
    let module =await Test.createTestingModule({
        controllers:[BenificiaryController],
        providers:[BenificiaryService,{
            provide:BenificiaryService,
            useFactory:()=>({
                addBenficiary: jest.fn(),
                getBenificiaryById: jest.fn(),
                deleteBenificiary:jest.fn(),
            })
        }]
    }).compile()
    benificiaryController=module.get<BenificiaryController>(BenificiaryController);
    benificiaryService=module.get<BenificiaryService>(BenificiaryService);
})
 it('should be defind',() =>{
     expect(benificiaryController).toBeDefined();
 })



 
 describe('When addBenficiary()', () =>{
    it('should return response',async ()=>{
   let addBenficiarySpy= jest.spyOn(benificiaryService, 'addBenficiary').mockResolvedValue(addBenficiary);
    let response = await benificiaryController.addBenficiary(benficiary)
     expect(response).toEqual(addBenficiary);
     expect(addBenficiarySpy).toHaveBeenCalledTimes(1);
    })
 })

 describe('When getBenificiaryById()', () =>{
   it('should return response',async ()=>{
    let getBenificiaryByIdSpy= jest.spyOn(benificiaryService, 'getBenificiaryById').mockResolvedValue(benficiary);
    let response = await benificiaryController.getBenificiaryById(1);
    expect(response).toEqual(benficiary);
    expect(getBenificiaryByIdSpy).toHaveBeenCalledTimes(1);
   })
})
 
 
describe('When deleteBenificiary()', () =>{
    it('should return response',async ()=>{
   let deleteBenificiarySpy= jest.spyOn(benificiaryService, 'deleteBenificiary').mockResolvedValue(deleteBenificiary);
    let response = await benificiaryController.deleteBenificiary(1);
     expect(response).toEqual(deleteBenificiary);
     expect(deleteBenificiarySpy).toHaveBeenCalledTimes(1);
    })
 })

})