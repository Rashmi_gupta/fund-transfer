import { User } from "../users/users.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * @author Rashmi Gupta
 * Accounts Entity
 */
@Entity()
export class Accounts {

    /**
     *Accounts Id will be Primary Genereted
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * ManyToOne relation with Accounts and User
     */
    @ManyToOne(() => User, (user: User) => user.accounts)
    user: User;

    /**
     * Bank Name will be string
     */
    @Column({ default: 'Union Bank of India' })
    bankName: string;          //default;

    /**
     * Branch Name will be string
     */
    @Column()
    branchName: string;

    /**
     * Type Of Account will be string
     */
    @Column()
    typeOfAccount: string;     //(Current or Saving or Loan)

    /**
     * Balance  will be number
     */
    @Column()
    balance: number;

    /**
     * IFSC_code will be a string
     */
    @Column()
    IFSC_code: string;

}