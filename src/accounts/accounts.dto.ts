import { IsNotEmpty } from "class-validator";
import { User } from "../users/users.entity";

/**
 * @author Rashmi Gupta
 * Accounts DTO
 */
export class AccountsDTO {

    /**
     * Accounts Id will be Primary Genereted Key
     */
    id: number;

    /**
     * User Id will be a Forgien Key
     */
    user: User;

    /**
     * Bank Name will be string
     */
    bankName: string;

    /**
     * Branch Name will be string
     */
    @IsNotEmpty({ message: 'Branch Name can not be empty' })
    branchName: string;

    /**
     * Type Of Account will be string
     */
    @IsNotEmpty({ message: 'Type Of Account can not be empty' })
    typeOfAccount: string;

    /**
     * Balance will be number
     */
    @IsNotEmpty({ message: 'balance can not be empty' })
    balance: number;

    /**
     * IFSC_code will be string
     */
    @IsNotEmpty({ message: 'IFSC_code can not be empty' })
    IFSC_code: string;


}