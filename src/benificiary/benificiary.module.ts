import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../users/users.entity";
import { UserRepository } from "../users/users.repository";
import { BenificiaryController } from "./benificiary.controller";
import { Benificiary } from "./benificiary.entity";
import { BenificiaryRepository } from "./benificiary.repository";
import { BenificiaryService } from "./benificiary.service";

/**
 * @author Rashmi Gupta
 * @Module BenificiaryModule
 */
@Module({
    imports: [
        TypeOrmModule.forFeature([BenificiaryRepository, Benificiary, UserRepository, User])
    ],
    controllers: [BenificiaryController],
    providers: [BenificiaryService]
})
export class BenificiaryModule { }