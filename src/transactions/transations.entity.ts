import { User } from "../users/users.entity";
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * @author Rashmi Gupta
 * Transactions Entity
 */
@Entity()
export class Transactions {

    /**
     * Transactions will be Primary Generated
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * ManyToOne Relation with Transactions and User
     */
    @ManyToOne(() => User, (user: User) => user.transactions)
    user: User;

    /**
     * To Id should be number
     */
    @Column()
    toId: number;

    /**
     * Transaction Type should be string
     */
    @Column()
    transactionType: string;        //Credit or Debit)

    /**
     * Amount should be number
     */
    @Column()
    amount: number;

    /**
     * Comments should be string
     */
    @Column()
    comments: string;

    /**
     * Date should be Date
     */
    @CreateDateColumn()
    Date: Date;

}