import { Test } from "@nestjs/testing";
import { OffersController } from "./offers.controller";
import { OffersService } from "./offers.service";

const addOffers="OFFERS ADDED SUCCESSFULLY WITH ID : 5"

const d = new Date(2018, 11, 24);

const offersOutput=
{
id: 3,
user:null,
offerName: "MyHome",
status: "Active",
offerStartDate: d,
offerEndDate: d,
offerDescription: "Home Loan"
 }

const deleteOffersById="ORDER DELETED SUCCESSFULLY WITH ID : 1";


describe('Given OffersController', () =>{
    let offersController: OffersController;
    let offersService:OffersService;
    
    beforeEach(async ()=>{
        let module =await Test.createTestingModule({
            controllers:[OffersController],
            providers:[OffersService,{
                provide:OffersService,
                useFactory:()=>({
                    addOffers: jest.fn(),
                    getOffersById: jest.fn(),
                    deleteOffersById:jest.fn(),
                })
            }]
        }).compile()
        offersController=module.get<OffersController>(OffersController);
        offersService=module.get<OffersService>(OffersService);
    })
     it('should be defind',() =>{
         expect(offersController).toBeDefined();
     })
    
    

     
     describe('When addOffers()', () =>{
        it('should return response',async ()=>{
       let addOffersSpy= jest.spyOn(offersService, 'addOffers').mockResolvedValue(addOffers);
        let response = await offersController.addOffers(offersOutput)
         expect(response).toEqual(addOffers);
         expect(addOffersSpy).toHaveBeenCalledTimes(1);
        })
     })
     

     describe('When getOffersById()', () =>{
       it('should return response',async ()=>{
      let getOffersByIdSpy= jest.spyOn(offersService, 'getOffersById').mockResolvedValue(offersOutput);
       let response = await offersController.getOffersById(1);
        expect(response).toEqual(offersOutput);
        expect(getOffersByIdSpy).toHaveBeenCalledTimes(1);
       })
    })
    
    
    
    describe('When deleteOffersById()', () =>{
        it('should return response',async ()=>{
       let deleteOffersByIdSpy= jest.spyOn(offersService, 'deleteOffersById').mockResolvedValue(deleteOffersById);
        let response = await offersController.deleteOffersById(1);
         expect(response).toEqual(deleteOffersById);
         expect(deleteOffersByIdSpy).toHaveBeenCalledTimes(1);
        })
     })
     
    
    
    })