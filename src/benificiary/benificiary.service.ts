import { ConflictException, Injectable, InternalServerErrorException, Logger, NotFoundException } from "@nestjs/common";
import { BENIFICIARY_ADDED_SUCCESS, BENIFICIARY_ADD_FAILED, BENIFICIARY_ALLREADY_EXIST, BENIFICIARY_BY_ID_SUCCESS, BENIFICIARY_DELETED_FAILURE, BENIFICIARY_DELETED_SUCCESSFULL, BENIFICIARY_NOT_FOUND } from "../constant";
import { UserRepository } from "../users/users.repository";
import { BenificiaryDTO } from "./benificiary.dto";
import { BenificiaryRepository } from "./benificiary.repository";

/**
 * @author Rashmi Gupta 
 * Benificiary Service
 */
@Injectable()
export class BenificiaryService {

    /**
     * Logger
     */
    logger = new Logger(BenificiaryService.name);

    /**
     * Dependency
     * @param benificiaryRepo BenificiaryRepository     
     * @param userRepo UserRepository
     */
    constructor(private benificiaryRepo: BenificiaryRepository,
        private readonly userRepo: UserRepository) { }

    /**
     * Add Benificiary
     * @param benificiary BenificiaryDTO
     * @returns Success or Failer
     */
    async addBenficiary(benificiary: BenificiaryDTO): Promise<string> {
        try {
            let userBenificiary = await this.benificiaryRepo.findOne({ relations: ["user"] });
            let response = await this.benificiaryRepo.save(benificiary);
            if (response) {
                const msg: string = `${BENIFICIARY_ADDED_SUCCESS} ${response.id}`;
                this.logger.log(msg)
                return msg;
            } else {
                throw new InternalServerErrorException(BENIFICIARY_ADD_FAILED)
            }
        } catch (error) {
            this.logger.error(error.message);
            if(error.errrno === 1062){
            throw new ConflictException(BENIFICIARY_ALLREADY_EXIST);
        }
        this.logger.error(error.message);
        throw new InternalServerErrorException(error.message)
        }
}

    /**
     * Benificiary by id
     * @param id number
     * @returns Success or Failure
     */
    async getBenificiaryById(id: number) :Promise<BenificiaryDTO>{
        try {
            let response = await this.benificiaryRepo.findOne(id);
            if (response) {
                return response ;
            } else {
                const msg = `${BENIFICIARY_NOT_FOUND}${id}`;
                throw new NotFoundException(msg)
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message)
        }
    }

    /**
     * Delete Benificiary by id
     * @param id number
     * @returns Success or Failure
     */
    async deleteBenificiary(id: number) :Promise<string>{
        try {
            let response = await this.benificiaryRepo.delete(id)
            if (response.affected > 0) {
                const msg = `${BENIFICIARY_DELETED_SUCCESSFULL}${id}`;
                return msg;
            } else {
                const msg = `${BENIFICIARY_DELETED_FAILURE} ${id}`;
                throw new NotFoundException(msg);
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message)
        }
    }

} 
