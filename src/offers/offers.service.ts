import { ConflictException, Injectable, InternalServerErrorException, Logger, NotFoundException } from "@nestjs/common";
import { OFFERS_ADD_FAILURE, OFFERS_ADD_SUCCESS, OFFER_ADD_CONFLICT, OFFER_ALREADY_EXIST, OFFER_BY_ID_DELETE_SUCCESS, OFFER_BY_ID_NOT_FOUND, OFFER_BY_ID_SUCCESS } from "../constant";
import { OffersDTO } from "./offers.dto";
import { OffersRepository } from "./offers.repository";

/**
 * @author Rashmi Gupta
 * Offers Service
 */
@Injectable()
export class OffersService {

    /**
     * Logger
     */
    logger = new Logger(OffersService.name);

    /**
     * Dependency
     * @param offersRepo OffersRepository
     */
    constructor(private readonly offersRepo: OffersRepository) { }

    /**
     * Add Offers
     * @param offers 
     * @returns Success Or Failure 
     */
    async addOffers(offers: OffersDTO): Promise<string> {
        try {
            let userOffers = await this.offersRepo.findOne({ relations: ["user"] });
            let response = await this.offersRepo.save(offers);
            if (response) {
                const msg: string = `${OFFERS_ADD_SUCCESS} ${response.id}`;
                return msg;
            } else {
                throw new InternalServerErrorException(OFFERS_ADD_FAILURE)
            }
        } catch (error) {
            this.logger.error(error.message);
            if(error.errrno === 1062){
            throw new ConflictException(OFFER_ALREADY_EXIST);
        }
        this.logger.error(error.message);
        throw new InternalServerErrorException(error.message)
        }
}
        
  

    /**
     * Get Offers By Id
     * @param id number
     * @returns Success or failure
     */
    async getOffersById(id: number):Promise<OffersDTO> {
        try {
            let response = await this.offersRepo.findOne(id);
            if (response) {
                return response;
            } else {
                const msg: string = `${OFFER_BY_ID_NOT_FOUND}${id}`;
                throw new NotFoundException(msg);
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message)
        }
    }

    /**
     * delete Offers By Id
     * @param id number
     * @returns Success or Failure
     */
    async deleteOffersById(id: number):Promise<string> {
        try {
            let response = await this.offersRepo.delete(id);
            if (response.affected > 0) {
                const msg:string = `${OFFER_BY_ID_DELETE_SUCCESS} ${id}`;
                return msg;
            } else {
                const msg = `${OFFER_BY_ID_NOT_FOUND} ${id}`;
                throw new NotFoundException(msg);
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message);
        }
    }

}