import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../users/users.entity";
import { UserRepository } from "../users/users.repository";
import { TransactionsRepository } from "./transactions.repository";
import { TransactionsController } from "./transactions.controller";
import { TransactionsService } from "./transactions.service";
import { Transactions } from "./transations.entity";

/**
 * @author Rashmi Gupta
 * Transactions Module
 */
@Module({
    imports: [
        TypeOrmModule.forFeature([TransactionsRepository, Transactions, UserRepository, User])],
    controllers: [TransactionsController],
    providers: [TransactionsService],
})
export class TransactionsModule { }