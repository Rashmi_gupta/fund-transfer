import { EntityRepository, Repository } from "typeorm";
import { Benificiary } from "./benificiary.entity";

/**
 * @author Rashmi Gupta 
 * Benificiary Repository
 */
@EntityRepository(Benificiary)
export class BenificiaryRepository extends Repository<Benificiary>{}