import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../users/users.entity";
import { UserRepository } from "../users/users.repository";
import { AccountsController } from "./accounts.controller";
import { Accounts } from "./accounts.entity";
import { AccountsRepository } from "./accounts.repository";
import { AccountsService } from "./accounts.service";

/**
 * @author Rashmi Gupta
 * @Module Accounts Module
 */
@Module({
    imports: [
        TypeOrmModule.forFeature([AccountsRepository, Accounts, User, UserRepository])
    ],
    controllers: [AccountsController],
    providers: [AccountsService],
})
export class AccountsModule { }