import { Injectable } from '@nestjs/common';

/**
 * AppService
 */
@Injectable()
export class AppService {

  /**
   * Get Hello
   * @returns string
   */
  getHello(): string {
    return 'Hello World!';
  }
}
