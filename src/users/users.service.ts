import { ConflictException, HttpException, HttpStatus, Injectable, InternalServerErrorException, Logger, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import * as bcrypt from 'bcryptjs';
import { INVALID_CREDENTIALS, USER_ALREADY_EXIST, USER_AUTHENTICATED, USER_NOT_REGISTER, USER_REGISTERD } from "../constant";
import { JwtPayload } from "./jwt/jwt-payload.interface";
import { LoginDTO } from "./login.dto";
import { UserDTO } from "./users.dto";
import { UserRepository } from "./users.repository";

/**
 * @author Rashmi Gupta
 * User Service
 */
@Injectable()
export class UserService {

  /**
   * Logger
   */
  logger = new Logger(UserService.name);

  /**
   * Dependency
   * @param userRepo UserRepository
   * @param jwtService JwtService
   */
  constructor(private userRepo: UserRepository,
    private jwtService: JwtService) { }

  /**
   * Register a new User
   * @param user UserDTO
   * @returns Success or Failure
   */
  async registerUser(user: UserDTO): Promise<string> {
    try {
      const { password } = user;
      const salt = await bcrypt.genSalt();
      const hashedPassword = await bcrypt.hash(password, salt);
      const response = await this.userRepo.save({ ...user, password: hashedPassword });
      if (response) {
        const message = `${USER_REGISTERD} ${response.id}`;
        return message;
      } else {
        throw new InternalServerErrorException(USER_NOT_REGISTER)
      }
    } catch (error) {
      this.logger.error(error.message);
      if (error.errrno === 1062) {
        throw new ConflictException(USER_ALREADY_EXIST);
      }
      this.logger.error(error.message);
      throw new InternalServerErrorException(error.message)
    }
  }

  /**
   * User Login
   * @param userLogin LoginDTO
   * @returns Success or Failure
   */
  async userLogin(userLogin: LoginDTO): Promise<any> {
    try {
      const userInfo = await this.userRepo.findOneOrFail({ emailId: userLogin.emailId });
      if (userInfo && await bcrypt.compare(userLogin.password, userInfo.password)) {
        const payload: JwtPayload = { emailId: userInfo.emailId };
        let token = this.jwtService.sign(payload);
        console.log(token)
        const msg = USER_AUTHENTICATED
        return { token, msg };
      } else {
        throw new UnauthorizedException(INVALID_CREDENTIALS)
      }
    } catch (error) {
      console.log(error)
      this.logger.error(error.message);
      if (error?.status === 401) {
        throw new UnauthorizedException(INVALID_CREDENTIALS)
      } else {
        throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR)
      }
    }
  }

}