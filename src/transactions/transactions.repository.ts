import { EntityRepository, Repository } from "typeorm";
import { Transactions } from "./transations.entity";

/**
 * Transactions Repository
 */
@EntityRepository(Transactions)
export class TransactionsRepository extends Repository<Transactions>{ }