'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">fund-transfer documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AccountsModule.html" data-type="entity-link" >AccountsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' : 'data-target="#xs-controllers-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' :
                                            'id="xs-controllers-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' }>
                                            <li class="link">
                                                <a href="controllers/AccountsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccountsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' : 'data-target="#xs-injectables-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' :
                                        'id="xs-injectables-links-module-AccountsModule-7eadfafe4bcc52e2837fa24c4bce561d8ac7e75aee229b299af682716b5456ef7cff3c3fcbac7bafcb76c12a16a539f1d0c8d225011de2870f37b18bd38bc629"' }>
                                        <li class="link">
                                            <a href="injectables/AccountsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccountsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' : 'data-target="#xs-controllers-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' :
                                            'id="xs-controllers-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' : 'data-target="#xs-injectables-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' :
                                        'id="xs-injectables-links-module-AppModule-daaf46ba6ccd1480637e0958b0fca78efd7500b4d322bc66d6e234f215ab87067f8b8647bc91a5c5c87b811daafebdb2909098ded6e1114301cd3611ccbf46d8"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BenificiaryModule.html" data-type="entity-link" >BenificiaryModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' : 'data-target="#xs-controllers-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' :
                                            'id="xs-controllers-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' }>
                                            <li class="link">
                                                <a href="controllers/BenificiaryController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BenificiaryController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' : 'data-target="#xs-injectables-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' :
                                        'id="xs-injectables-links-module-BenificiaryModule-b6b8cb339152f6d14c9886385930a898e3a28f5425e9d2790e38c6bc0e585c3043b65c30e5888c2399e7c8257918ab93ebfb764d94f8c78a726fb38c29359376"' }>
                                        <li class="link">
                                            <a href="injectables/BenificiaryService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BenificiaryService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OffersModule.html" data-type="entity-link" >OffersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' : 'data-target="#xs-controllers-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' :
                                            'id="xs-controllers-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' }>
                                            <li class="link">
                                                <a href="controllers/OffersController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OffersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' : 'data-target="#xs-injectables-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' :
                                        'id="xs-injectables-links-module-OffersModule-0847485e00ed1ce0f9089d9011fe4267abb56730ad041de5de9b716445d005787180375a871c6bc1a6171f058ee2e7af71b75b7d07044cab58169b45a96ceb0d"' }>
                                        <li class="link">
                                            <a href="injectables/OffersService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OffersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TransactionsModule.html" data-type="entity-link" >TransactionsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' : 'data-target="#xs-controllers-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' :
                                            'id="xs-controllers-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' }>
                                            <li class="link">
                                                <a href="controllers/TransactionsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TransactionsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' : 'data-target="#xs-injectables-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' :
                                        'id="xs-injectables-links-module-TransactionsModule-84e2a7fad5806c16f22b6eee110390e140bcaa5c0b4075040a42a16058e6eb733258512ac671cff481bed710dc74d73d9a8485d68ad483015339204b769f1ab6"' }>
                                        <li class="link">
                                            <a href="injectables/TransactionsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TransactionsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' : 'data-target="#xs-controllers-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' :
                                            'id="xs-controllers-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' : 'data-target="#xs-injectables-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' :
                                        'id="xs-injectables-links-module-UserModule-8a30e0544a369982abe2e5fabdd829c3b8c4bc9d419819b8831979e7a09c5c006ba17e07767025c8a746ec6df317f71b2f57654393e87e45868e78e015fc852d"' }>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AccountsController.html" data-type="entity-link" >AccountsController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BenificiaryController.html" data-type="entity-link" >BenificiaryController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/OffersController.html" data-type="entity-link" >OffersController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/TransactionsController.html" data-type="entity-link" >TransactionsController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UserController.html" data-type="entity-link" >UserController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Accounts.html" data-type="entity-link" >Accounts</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Benificiary.html" data-type="entity-link" >Benificiary</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Offers.html" data-type="entity-link" >Offers</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Transactions.html" data-type="entity-link" >Transactions</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AccountsDTO.html" data-type="entity-link" >AccountsDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/AccountsRepository.html" data-type="entity-link" >AccountsRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/BenificiaryDTO.html" data-type="entity-link" >BenificiaryDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/BenificiaryRepository.html" data-type="entity-link" >BenificiaryRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoginDTO.html" data-type="entity-link" >LoginDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/OffersDTO.html" data-type="entity-link" >OffersDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/OffersRepository.html" data-type="entity-link" >OffersRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/TransactionsDTO.html" data-type="entity-link" >TransactionsDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/TransactionsRepository.html" data-type="entity-link" >TransactionsRepository</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserDTO.html" data-type="entity-link" >UserDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserRepository.html" data-type="entity-link" >UserRepository</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AccountsService.html" data-type="entity-link" >AccountsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BenificiaryService.html" data-type="entity-link" >BenificiaryService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtAuthGard.html" data-type="entity-link" >JwtAuthGard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtStrategy.html" data-type="entity-link" >JwtStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OffersService.html" data-type="entity-link" >OffersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ResponseInterceptor.html" data-type="entity-link" >ResponseInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TransactionsService.html" data-type="entity-link" >TransactionsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/JwtPayload.html" data-type="entity-link" >JwtPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Response.html" data-type="entity-link" >Response</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});