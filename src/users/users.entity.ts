import { Accounts } from "../accounts/accounts.entity";
import { Benificiary } from "../benificiary/benificiary.entity";
import { Offers } from "../offers/offers.entity";
import { Transactions } from "../transactions/transations.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

/**
 * @author Rashmi Gupta
 * User Entity
 */
@Entity()
export class User {

    /**
     *UserId will be Primary Generated
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * userName should be string
     */
    @Column()
    userName: string;

    /**
     * should be string
     */
    @Column({ unique: true })
    emailId: string;

    /**
     * password should be string
     */
    @Column()
    password: string;

    /**
     * phoneNumber should be number
     */
    @Column()
    phoneNumber: number;

    /**
     * address should be string
     */
    @Column()
    address: string;

    /**
     * OneToMany relation with User and Accounts
     */
    @OneToMany(() => Accounts, (account: Accounts) => account.user)
    accounts: Accounts[];

    /**
     * OneToMany relation with User and Benificiary
     */
    @OneToMany(() => Benificiary, (benificiary: Benificiary) => benificiary.user)
    benficiary: Benificiary[];

    /**
     * OneToMany relation with User and Offers
     */
    @OneToMany(() => Offers, (offers: Offers) => offers.user)
    offers: Offers[];

    /**
     *  OneToMany relation with User and Transactions
     */
    @OneToMany(() => Transactions, (transactions: Transactions) => transactions.user)
    transactions: Transactions[];


}