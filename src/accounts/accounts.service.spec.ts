const accountAdd=
{
    "data": "ACCOUNT CREATED SUCCESSFULLY WITH ID : 7"
  }
 
const account=
{
id: 1,
user: null,
bankName: "Union Bank of India",
branchName: "Noida",
typeOfAccount: "Saving",
balance: 10000,
IFSC_code: "123abc"
}

const accountDelete="ACCOUNT DELETED SUCCESSFULLY WITH ID : 1";


describe('BenificiaryService', () => {
    let benificiaryService: BenificiaryService;
    let benificiaryRepo;

    beforeEach(async () => {
        let module: TestingModule = await Test.createTestingModule({
            providers: [BenificiaryService, {
                provide: BenificiaryRepository,
                useFactory: () => ({
                    addBenficiary: jest.fn(),
                    getBenificiaryById: jest.fn(),
                    deleteBenificiary: jest.fn(),
                })
            }]
        }).compile();

        benificiaryService = module.get<BenificiaryService>(BenificiaryService);
        benificiaryRepo = module.get<BenificiaryRepository>(BenificiaryRepository);
    });

    it("should be defined", () => {
        expect(benificiaryService).toBeDefined();
    });

     /**
      * Add Benficiary
      */
    describe("When addBenficiary()", () => {
        describe("AND success", () => {
            it("should return response", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'addBenficiary').mockResolvedValue(addBenficiary);
                let response = await benificiaryService.addBenficiary(benficiary);
                expect(response).toEqual(addBenficiary);
                expect(findSpy).toHaveBeenCalled();
                expect(findSpy).toHaveBeenCalledTimes(1);
            })
        })
        describe("AND Failed", () => {
            it("should return error", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'find').mockRejectedValue(addBenficiaryFailed);
                let response = await benificiaryService.addBenficiary(benficiary);
                expect(response).rejects.toThrow(InternalServerErrorException);
                expect(findSpy).toHaveBeenCalled();
            })
        })
    })


    /**
     * Get Benificiary By Id
     */
    describe("When getBenificiaryById()", () => {
        describe("AND success", () => {
            it("should return response", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'getBenificiaryById').mockResolvedValue(benficiary);
                let response = await benificiaryService.getBenificiaryById(1);
                expect(response).toEqual(benficiary);
                expect(findSpy).toHaveBeenCalled();
                expect(findSpy).toHaveBeenCalledTimes(1);
            })
        })
        describe("AND Failed", () => {
            it("should return error", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'find').mockRejectedValue(benficiaryByIdFailed);
                let response = await benificiaryService.getBenificiaryById(1);
                expect(response).rejects.toThrow(NotFoundException);
                expect(findSpy).toHaveBeenCalled();
            })
        })
    })

    /**
     * Delete Benificiary
     */
    describe("When deleteBenificiary()", () => {
        describe("AND success", () => {
            it("should return response", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'deleteBenificiary').mockResolvedValue(deleteBenificiary);
                let response = await benificiaryService.deleteBenificiary(1);
                expect(response).toEqual(deleteBenificiary);
                expect(findSpy).toHaveBeenCalled();
                expect(findSpy).toHaveBeenCalledTimes(1);
            })
        })
        describe("AND Failed", () => {
            it("should return error", async () => {
                let findSpy = jest.spyOn(benificiaryRepo, 'find').mockRejectedValue(deleteBenificiaryFailed);
                let response = await benificiaryService.deleteBenificiary(1);
                expect(response).rejects.toThrow(NotFoundException);
                expect(findSpy).toHaveBeenCalled();
            })
        })
    })


})