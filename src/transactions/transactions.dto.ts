import { IsNotEmpty, IsNumber, IsString } from "class-validator";
import { User } from "../users/users.entity";

/**
 * @author Rashmi Gupta
 * Transactions DTO
 */
export class TransactionsDTO {

    /**
     * Transactions Id will be Primary Generated
     */
    id: number;

    /**
     * User Id will be Forgien  Key
     */
    user: User;

    /**
     * To Id will be number
     */
    @IsNumber()
    @IsNotEmpty({ message: 'To Id can not be empty' })
    toId: number;

    /**
     * Transaction Type will be string
     */
    @IsString({ message: 'Transaction Type must be a string' })
    @IsNotEmpty({ message: 'Transaction Type can not be empty' })
    transactionType: string;

    /**
     * Amount will be number
     */
    @IsNumber()
    @IsNotEmpty({ message: 'Amount can not be empty' })
    amount: number;

    /**
     * Comments will be string
     */
    @IsNotEmpty({ message: ' can not be empty' })
    comments: string;

    /**
     * Date will Auto genrate when transaction occure
     */
    Date: Date;

}