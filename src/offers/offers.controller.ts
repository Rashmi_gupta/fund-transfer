import { Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Post } from "@nestjs/common";
import {  ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse } from "@nestjs/swagger";
import { BENIFICIARY_BY_ID_FAILED, BENIFICIARY_BY_ID_SUCCESS, BENIFICIARY_DELETED_FAILURE, BENIFICIARY_DELETED_SUCCESSFULL, OFFERS_ADD_FAILURE, OFFERS_ADD_SUCCESS } from "../constant";
import { OffersDTO } from "./offers.dto";
import { OffersService } from "./offers.service";

/**
 * @author Rashmi Gupta
 * Offers Controller
 */
@Controller('user')
export class OffersController {
 
    /**
     * Dependency
     * @param offersServie OffersService
     */
    constructor(private offersServie: OffersService) { }
    
    /**
     * Add Offers
     * @param offers OffersDTO
     * @returns Success or Failure
     */
    @ApiCreatedResponse({ description: OFFERS_ADD_SUCCESS, status: HttpStatus.CREATED })
    @ApiInternalServerErrorResponse({ description: OFFERS_ADD_FAILURE, status: HttpStatus.INTERNAL_SERVER_ERROR })
    @Post("addOffers")
    addOffers(@Body() offers:OffersDTO):Promise<string>{
        return this.offersServie.addOffers(offers);
    }

    /**
     * Get Offers By Id
     * @param id number
     * @returns Success or Failure
     */
    @ApiOkResponse({description:BENIFICIARY_BY_ID_SUCCESS,status:HttpStatus.OK})
    @ApiInternalServerErrorResponse({description:BENIFICIARY_BY_ID_FAILED,status:HttpStatus.INTERNAL_SERVER_ERROR})
    @Get(':id/offers') 
    getOffersById (@Param('id',ParseIntPipe)id:number):Promise<OffersDTO>{
        return this.offersServie.getOffersById(id);
    }

    /**
     * Delete Offers By Id
     * @param id number
     * @returns Success or Failure
     */
    @ApiOkResponse({description:BENIFICIARY_DELETED_SUCCESSFULL,status:HttpStatus.OK})
    @ApiInternalServerErrorResponse({description:BENIFICIARY_DELETED_FAILURE,status:HttpStatus.INTERNAL_SERVER_ERROR})
    @Delete(':id/deleteOffers')
    deleteOffersById(@Param('id',ParseIntPipe)id:number):Promise<string>{
     return this.offersServie.deleteOffersById(id);
    }

}