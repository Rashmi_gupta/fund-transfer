import { User } from "../users/users.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * @author Rashmi Gupta
 * Offers Entity
 */
@Entity()
export class Offers {

    /**
     * Offers Id will be Primary Genereted
     */
    @PrimaryGeneratedColumn()
    id: number;

    /**
     * ManyToOne
     */
    @ManyToOne(() => User, (user: User) => user.offers)
    user: User;

    /**
     * offerName
     */
    @Column()
    offerName: string;

    /**
     * status
     */
    @Column()
    status: string;

    /**
     * offerStartDate
     */
    @Column()
    offerStartDate: Date;

    /**
     * offerEndDate
     */
    @Column()
    offerEndDate: Date;

    /**
     * offerDescription
     */
    @Column()
    offerDescription: string;

}
