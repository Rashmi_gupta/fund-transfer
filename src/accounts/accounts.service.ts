import { ConflictException, Injectable, InternalServerErrorException, Logger, NotFoundException } from "@nestjs/common";
import { ACCOUNT_ALREADY_EXIST, ACCOUNT_BY_ID_NOT_FOUND, ACCOUNT_CREATED_SUCCESS, ACCOUNT_CREATION_FAILED, ACCOUNT_DELETE_BY_ID_SUCCESS, ACCOUNT_FETCHED_BY_ID, ACCOUNT_FETCHED_BY_ID_FAILID } from "../constant";
import { UserRepository } from "../users/users.repository";
import { AccountsDTO } from "./accounts.dto";
import { AccountsRepository } from "./accounts.repository";

/**
 * @author Rashmi Gupta
 * Accounts Service
 */
@Injectable()
export class AccountsService {

    /**
     * Logger
     */
    logger = new Logger(AccountsService.name);

    /**
     * Dependency Injection
     * @param accountsRepo AccountsRepository
     */
    constructor(private accountsRepo: AccountsRepository,
        private readonly userRepo: UserRepository) { }

    /**
     * Create a Account
     * @param accounts AccountsDTO
     * @returns Success or failer
     */
    async createAccount(accounts: AccountsDTO): Promise<any> {
        try {
            let userAccount = await this.accountsRepo.findOne({ relations: ["user"] });
            let response = await this.accountsRepo.save(accounts);
            if (response) {
                const msg = `${ACCOUNT_CREATED_SUCCESS} ${response.id}`;
                this.logger.log(msg)
                return msg;
            } else {
                throw new InternalServerErrorException(ACCOUNT_CREATION_FAILED)
            }
        } catch (error) {
            this.logger.error(error.message);
            if(error.errrno === 1062){
            throw new ConflictException(ACCOUNT_ALREADY_EXIST);
        }
        this.logger.error(error.message);
        throw new InternalServerErrorException(error.message)
        }
} 

    /**
     * Get Account by Id
     * @param id id:number
     * @returns Success or Failure
     */
    async getAccountById(id: number): Promise<any> {
        try {
            let response= await this.userRepo.findOne(id,{relations:['accounts']});
            if (response) {
                //this.logger.log(`${ACCOUNT_FETCHED_BY_ID} ${id}`);
                return response;
            } else {
                const msg = `${ACCOUNT_FETCHED_BY_ID_FAILID} ${id}`;
                throw new NotFoundException(msg);
            }
        } catch (error) {
            this.logger.error(error.message);
            throw new InternalServerErrorException(error.message)
        }
    }

    /** 
     * Delete Account By Id
     * @param id number
     * @returns Success or Failure
     */
    async deleteAccountById(id:number){
        try{
              let response =await this.accountsRepo.delete(id);
              if(response.affected>0){
                  const msg =`${ACCOUNT_DELETE_BY_ID_SUCCESS} ${id}`;
                  return msg;
              }else{
                  const msg =`${ACCOUNT_BY_ID_NOT_FOUND} ${id}`; 
                  throw new NotFoundException(msg);
              }
        }catch(error){
             this.logger.error(error.message);
             throw new InternalServerErrorException(error.message);
        }
    }

} 