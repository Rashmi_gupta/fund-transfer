import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountsModule } from './accounts/accounts.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BenificiaryModule } from './benificiary/benificiary.module';
import { OffersModule } from './offers/offers.module';
import { TransactionsModule } from './transactions/transactions.module';
import { UserModule } from './users/users.module';

/**
 * AppModule(Root Module)
 */
@Module({
  imports: [TypeOrmModule.forRoot(),
    UserModule,
    BenificiaryModule,
    AccountsModule,
    TransactionsModule,
    OffersModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
