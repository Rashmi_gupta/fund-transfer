import { Controller, Get, HttpStatus, Param, ParseIntPipe } from "@nestjs/common";
import { ApiInternalServerErrorResponse, ApiOkResponse } from "@nestjs/swagger";
import { TRANSCACTION_BY_ID_SUCCESS, TRANSCTION_BY_iD_FAILURE } from "../constant";
import { TransactionsDTO } from "./transactions.dto";
import { TransactionsService } from "./transactions.service";

/**
 * Transactions Controller
 */
@Controller('user')
export class TransactionsController {

    /**
     * Dependency
     * @param transactionsService TransactionsService
     */
    constructor(private transactionsService: TransactionsService) { }

    /**
     * Get Transaction with Id
     * @param id 
     * @returns 
     */
    @ApiOkResponse({ description: TRANSCACTION_BY_ID_SUCCESS, status: HttpStatus.OK })
    @ApiInternalServerErrorResponse({ description: TRANSCTION_BY_iD_FAILURE, status: HttpStatus.INTERNAL_SERVER_ERROR })
    @Get(':id/transaction')
    getTransactionById(@Param('id', ParseIntPipe) id: number): Promise<TransactionsDTO> {
        return this.getTransactionById(id);
    }


}