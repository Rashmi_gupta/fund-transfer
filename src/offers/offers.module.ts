import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../users/users.entity";
import { UserRepository } from "../users/users.repository";
import { OffersController } from "./offers.controller";
import { Offers } from "./offers.entity";
import { OffersRepository } from "./offers.repository";
import { OffersService } from "./offers.service";

/**
 * @author Rashmi Gupta
 * Offers Module
 */
@Module({
    imports: [
        TypeOrmModule.forFeature([OffersRepository, Offers, User, UserRepository])
    ],
    controllers: [OffersController],
    providers: [OffersService]
})
export class OffersModule { } 