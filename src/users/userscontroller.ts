import { Body, Controller, HttpStatus, Post } from "@nestjs/common";
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse } from "@nestjs/swagger";
import { INVALID_CREDENTIALS, LOG_IN_SUCCESS, USER_NOT_REGISTER, USER_REGISTERD } from "../constant";
import { LoginDTO } from "./login.dto";
import { UserDTO } from "./users.dto";
import { UserService } from "./users.service";

/**
 * @author Rashmi Gupta
 * User Controller
 */
@Controller('user')
export class UserController {

    /**
     * Dependency 
     * @param userService UserService
     */
    constructor(private readonly userService: UserService) { }

    /**
     * Register User
     * @param userDto UserDTO
     * @returns Success or Failure
     */
    @ApiCreatedResponse({ description: USER_REGISTERD, status: HttpStatus.CREATED })
    @ApiInternalServerErrorResponse({ description: USER_NOT_REGISTER, status: HttpStatus.INTERNAL_SERVER_ERROR })
    @Post('/register')
    registerUser(@Body() userDto: UserDTO): Promise<string> {
        return this.userService.registerUser(userDto)
    }

    /**
     * User Login
     * @param userLogin LoginDTO
     * @returns Success or Failure
     */
    @ApiOkResponse({ description: LOG_IN_SUCCESS, status: HttpStatus.CREATED })
    @ApiInternalServerErrorResponse({ description: INVALID_CREDENTIALS, status: HttpStatus.INTERNAL_SERVER_ERROR })
    @Post('/login')
    userLogin(@Body() userLogin: LoginDTO): Promise<{ token }> {
        return this.userService.userLogin(userLogin);
    }

}
