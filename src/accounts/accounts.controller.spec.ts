import { Test } from "@nestjs/testing";
import { AccountsController } from "./accounts.controller";
import { AccountsService } from "./accounts.service";

const accountAdd=
{
    "data": "ACCOUNT CREATED SUCCESSFULLY WITH ID : 7"
  }
 
const account=
{
id: 1,
user: null,
bankName: "Union Bank of India",
branchName: "Noida",
typeOfAccount: "Saving",
balance: 10000,
IFSC_code: "123abc"
}

const accountDelete="ACCOUNT DELETED SUCCESSFULLY WITH ID : 1";


describe('Given AccountsController', () =>{
    let accountsController: AccountsController;
    let accountsService:AccountsService;
    
    beforeEach(async ()=>{
        let module =await Test.createTestingModule({
            controllers:[AccountsController],
            providers:[AccountsService,{
                provide:AccountsService,
                useFactory:()=>({
                    createAccount: jest.fn(),
                    getAccountById: jest.fn(),
                    deleteAccountById:jest.fn(),
                })
            }]
        }).compile()
        accountsController=module.get<AccountsController>(AccountsController);
        accountsService=module.get<AccountsService>(AccountsService);
    })
     it('should be defind',() =>{
         expect(accountsController).toBeDefined();
     })
    
    

     
     describe('When createAccount()', () =>{
        it('should return response',async ()=>{
       let createAccountSpy= jest.spyOn(accountsService, 'createAccount').mockResolvedValue(account);
        let response = await accountsController.createAccount(account)
         expect(response).toEqual(accountAdd);
         expect(createAccountSpy).toHaveBeenCalledTimes(1);
        })
     })
     

     describe('When getAccountById()', () =>{
       it('should return response',async ()=>{
      let getAccountByIdSpy= jest.spyOn(accountsService, 'getAccountById').mockResolvedValue(account);
       let response = await accountsController.getAccountById(1);
        expect(response).toEqual(account);
        expect(getAccountByIdSpy).toHaveBeenCalledTimes(1);
       })
    })
    
    
    describe('When deleteAccountById()', () =>{
        it('should return response',async ()=>{
       let deleteAccountByIdSpy= jest.spyOn(accountsService, 'deleteAccountById').mockResolvedValue(accountDelete);
        let response = await accountsController.deleteAccountById(3);
         expect(response).toEqual(accountDelete);
         expect(deleteAccountByIdSpy).toHaveBeenCalledTimes(1);
        })
     })
     
    
    
    })