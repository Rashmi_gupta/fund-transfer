import { EntityRepository, Repository } from "typeorm";
import { Accounts } from "./accounts.entity";

/**
 * @author Rashmi Gupta
 * Accounts Repository
 */
@EntityRepository(Accounts)
export class AccountsRepository extends Repository<Accounts>{ }