import { IsNotEmpty, IsNumber } from "class-validator";
import { User } from "../users/users.entity";

/**
 * @author Rashmi Gupta
 * Benificiary DTO
 */
export class BenificiaryDTO {

    /**
     * Benificiary Id will be Primary Generated
     */
    id: number;

    /**
     * User Id will be a Forgien key
     */
    user: User;

    /**
     * Benificiary IFSC will be a string
     */
    @IsNotEmpty({ message: 'Benificiary IFSC can not be empty' })
    benificiaryIFSC: string;

    /**
     * Status will be string
     */
    @IsNotEmpty({ message: 'status can not be empty' })
    status: string;

    /**
     * Benificiary Account Number will be number
     */
    @IsNotEmpty({ message: 'Benificiary Account Number can not be empty' })
    @IsNumber()
    benificiaryAccountNumber: number;

}