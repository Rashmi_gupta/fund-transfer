import { Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Post } from "@nestjs/common";
import { ApiCreatedResponse, ApiInternalServerErrorResponse, ApiOkResponse } from '@nestjs/swagger';
import { BENIFICIARY_ADDED_SUCCESS, BENIFICIARY_ADD_FAILED, BENIFICIARY_BY_ID_FAILED, BENIFICIARY_BY_ID_SUCCESS, BENIFICIARY_DELETED_FAILURE, BENIFICIARY_DELETED_SUCCESSFULL, FETCHED_SUCCESSFULLY, FETCH_BENIFICIARY_BY_ID } from "../constant";
import { BenificiaryDTO } from "./benificiary.dto";
import { BenificiaryService } from "./benificiary.service";

/**
 * @author Rashmi Gupta
 * Benificiary Controller
 */
@Controller('user')
export class BenificiaryController {

    /**
     * Dependency
     * @param benificiaryService BenificiaryService
     */
    constructor(private readonly benificiaryService: BenificiaryService) { }

    /**
     * Add Benficiary
     * @param benificiary BenificiaryDTO
     * @returns Success or Failure
     */
    @ApiCreatedResponse({ description: BENIFICIARY_ADDED_SUCCESS, status: HttpStatus.CREATED })
    @ApiInternalServerErrorResponse({ description: BENIFICIARY_ADD_FAILED, status: HttpStatus.INTERNAL_SERVER_ERROR })
    @Post("addBenificiary")
    addBenficiary(@Body() benificiary: BenificiaryDTO):Promise<string>{
        return this.benificiaryService.addBenficiary(benificiary)
    }

    /**
     * Get Benificiary By Id
     * @param id number
     * @returns Success or Failure
     */
    @ApiOkResponse({description:BENIFICIARY_BY_ID_SUCCESS,status:HttpStatus.OK})
    @ApiInternalServerErrorResponse({description:BENIFICIARY_BY_ID_FAILED,status:HttpStatus.INTERNAL_SERVER_ERROR})
    @Get(':id/benificiary')
    getBenificiaryById(@Param('id',ParseIntPipe)id:number):Promise<BenificiaryDTO>{
        return this.benificiaryService.getBenificiaryById(id);
    }

    /**
     * Delete Benificiary
     * @param id number
     * @returns Success or Failure
     */
    @ApiOkResponse({description:BENIFICIARY_DELETED_SUCCESSFULL,status:HttpStatus.OK})
    @ApiInternalServerErrorResponse({description:BENIFICIARY_DELETED_FAILURE,status:HttpStatus.INTERNAL_SERVER_ERROR})
    @Delete(':id/deleteBenificiary')
    deleteBenificiary(@Param('id',ParseIntPipe) id:number):Promise<string>{
        return this.benificiaryService.deleteBenificiary(id);
    }

}