import { EntityRepository, Repository } from "typeorm";
import { User } from "./users.entity";

/**
 * @author Rashmi Gupta
 * User Repository
 */
@EntityRepository(User)
export class UserRepository extends Repository<User>{ }