import { User } from "../users/users.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

/**
 * @author Rashmi Gupta 
 * Benificiary Entitity
 */
@Entity()
export class Benificiary {

  /**
   * Benificiary id
    */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * ManyToOne relation with Benificiary and User 
   */
  @ManyToOne(() => User, (user: User) => user.benficiary)
  user: User;

  /**
   * Benificiary IFSC will be string
   */
  @Column()
  benificiaryIFSC: string;

  /**
   * Status will be string
   */
  @Column()
  status: string;

  /**
   * Benificiary Account Number
   */
  @Column()
  benificiaryAccountNumber: number;

}