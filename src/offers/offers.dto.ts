import { IsNotEmpty } from "class-validator";
import { User } from "../users/users.entity";

/**
 * @author Rashmi Gupta
 * Offers DTO
 */
export class OffersDTO {

    /**
     * Offers Id will be Primary Generated
     */
    id: number;

    /**
     * User Id will be Forgien Key
     */
    user: User;

    /**
     * OfferName sholud be string
     */
    @IsNotEmpty({ message: 'Offer Name can not be empty' })
    offerName: string;

    /**
     * Status should be string
     */
    @IsNotEmpty({ message: 'status can not be empty' })
    status: string;

    /**
     * Offer Start Date will be Date
     */
    @IsNotEmpty({ message: 'OfferStart Date can not be empty' })
    offerStartDate: Date;

    /**
     * Offer End Date will be Date
     */
    @IsNotEmpty({ message: 'Offer End Date can not be empty' })
    offerEndDate: Date;

    /**
     * Offer Description should be string
     */
    @IsNotEmpty({ message: 'Offer Description  can not be empty' })
    offerDescription: string;

} 